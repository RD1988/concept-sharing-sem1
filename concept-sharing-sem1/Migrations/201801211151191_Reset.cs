namespace concept_sharing_sem1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Reset : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Concepts",
                c => new
                    {
                        ConceptID = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                        JsonPath = c.String(),
                        CourseID = c.Int(nullable: false),
                        SourceID = c.Int(),
                    })
                .PrimaryKey(t => t.ConceptID)
                .ForeignKey("dbo.Courses", t => t.CourseID, cascadeDelete: true)
                .Index(t => t.CourseID);
            
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        CourseID = c.Int(nullable: false, identity: true),
                        CourseName = c.String(),
                    })
                .PrimaryKey(t => t.CourseID);
            
            CreateTable(
                "dbo.Sources",
                c => new
                    {
                        SourceID = c.Int(nullable: false, identity: true),
                        Arthor = c.String(),
                        SourceName = c.String(),
                        ConceptID = c.Int(),
                        SourceTypeID = c.Int(),
                    })
                .PrimaryKey(t => t.SourceID)
                .ForeignKey("dbo.Concepts", t => t.ConceptID)
                .ForeignKey("dbo.SourceTypes", t => t.SourceTypeID)
                .Index(t => t.ConceptID)
                .Index(t => t.SourceTypeID);
            
            CreateTable(
                "dbo.SourceTypes",
                c => new
                    {
                        SourceTypeID = c.Int(nullable: false, identity: true),
                        SourceTypeName = c.String(),
                    })
                .PrimaryKey(t => t.SourceTypeID);
            
            CreateTable(
                "dbo.Subjects",
                c => new
                    {
                        SubjectID = c.Int(nullable: false),
                        Title = c.String(nullable: false, maxLength: 150),
                    })
                .PrimaryKey(t => t.SubjectID)
                .ForeignKey("dbo.Concepts", t => t.SubjectID, cascadeDelete: true)
                .Index(t => t.SubjectID)
                .Index(t => t.Title, unique: true, name: "TitleIndex");
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        Email = c.String(maxLength: 150),
                        RoleID = c.Int(),
                    })
                .PrimaryKey(t => t.UserID)
                .ForeignKey("dbo.Roles", t => t.RoleID)
                .Index(t => t.Email, unique: true, name: "EmailIndex")
                .Index(t => t.RoleID);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        RoleID = c.Int(nullable: false, identity: true),
                        RoleName = c.String(),
                    })
                .PrimaryKey(t => t.RoleID);
            
            CreateTable(
                "dbo.Rights",
                c => new
                    {
                        RightID = c.Int(nullable: false, identity: true),
                        RightName = c.String(),
                    })
                .PrimaryKey(t => t.RightID);
            
            CreateTable(
                "dbo.SourceTypeAttributes",
                c => new
                    {
                        SourceTypeAttributeID = c.Int(nullable: false, identity: true),
                        SourceTypeAttributeName = c.String(),
                        SourceTypeAttributeCatID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SourceTypeAttributeID);
            
            CreateTable(
                "dbo.UserConcepts",
                c => new
                    {
                        User_UserID = c.Int(nullable: false),
                        Concept_ConceptID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_UserID, t.Concept_ConceptID })
                .ForeignKey("dbo.Users", t => t.User_UserID, cascadeDelete: true)
                .ForeignKey("dbo.Concepts", t => t.Concept_ConceptID, cascadeDelete: true)
                .Index(t => t.User_UserID)
                .Index(t => t.Concept_ConceptID);
            
            CreateTable(
                "dbo.RightRoles",
                c => new
                    {
                        Right_RightID = c.Int(nullable: false),
                        Role_RoleID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Right_RightID, t.Role_RoleID })
                .ForeignKey("dbo.Rights", t => t.Right_RightID, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.Role_RoleID, cascadeDelete: true)
                .Index(t => t.Right_RightID)
                .Index(t => t.Role_RoleID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "RoleID", "dbo.Roles");
            DropForeignKey("dbo.RightRoles", "Role_RoleID", "dbo.Roles");
            DropForeignKey("dbo.RightRoles", "Right_RightID", "dbo.Rights");
            DropForeignKey("dbo.UserConcepts", "Concept_ConceptID", "dbo.Concepts");
            DropForeignKey("dbo.UserConcepts", "User_UserID", "dbo.Users");
            DropForeignKey("dbo.Subjects", "SubjectID", "dbo.Concepts");
            DropForeignKey("dbo.Sources", "SourceTypeID", "dbo.SourceTypes");
            DropForeignKey("dbo.Sources", "ConceptID", "dbo.Concepts");
            DropForeignKey("dbo.Concepts", "CourseID", "dbo.Courses");
            DropIndex("dbo.RightRoles", new[] { "Role_RoleID" });
            DropIndex("dbo.RightRoles", new[] { "Right_RightID" });
            DropIndex("dbo.UserConcepts", new[] { "Concept_ConceptID" });
            DropIndex("dbo.UserConcepts", new[] { "User_UserID" });
            DropIndex("dbo.Users", new[] { "RoleID" });
            DropIndex("dbo.Users", "EmailIndex");
            DropIndex("dbo.Subjects", "TitleIndex");
            DropIndex("dbo.Subjects", new[] { "SubjectID" });
            DropIndex("dbo.Sources", new[] { "SourceTypeID" });
            DropIndex("dbo.Sources", new[] { "ConceptID" });
            DropIndex("dbo.Concepts", new[] { "CourseID" });
            DropTable("dbo.RightRoles");
            DropTable("dbo.UserConcepts");
            DropTable("dbo.SourceTypeAttributes");
            DropTable("dbo.Rights");
            DropTable("dbo.Roles");
            DropTable("dbo.Users");
            DropTable("dbo.Subjects");
            DropTable("dbo.SourceTypes");
            DropTable("dbo.Sources");
            DropTable("dbo.Courses");
            DropTable("dbo.Concepts");
        }
    }
}
