namespace concept_sharing_sem1.Migrations
{
    using concept_sharing_sem1.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<concept_sharing_sem1.Models.Connection>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        // use Enable-Migrations in package manager if migrations dont exist
        // use Add-Migration to add a migration and remember a name. Example = Add-Migration ""Initial" <-- Name
        // Use Update-Database to update with the new migration you just have added



        protected override void Seed(concept_sharing_sem1.Models.Connection context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.


            //This is test data
            context.Users.AddOrUpdate(x => x.UserID, new User() { UserID = 1, Email = "rd@mail.dk", Username = "RD", Password = "123" });

            context.Users.AddOrUpdate(x => x.UserID, new User() { UserID = 2, Email = "sb@mail.dk", Username = "SB", Password = "123" });

            context.Users.AddOrUpdate(x => x.UserID, new User() { UserID = 3, Email = "pr@mail.dk", Username = "PR", Password = "123" });

            context.Roles.AddOrUpdate(x => x.RoleID, new Role()
            {
                RoleID = 1,
                RoleName = "Admin"
            }
            );

            context.Roles.AddOrUpdate(x => x.RoleID, new Role()
            {
                RoleID = 2,
                RoleName = "Teacher"
            }
         );

            context.Roles.AddOrUpdate(x => x.RoleID, new Role()
            {
                RoleID = 3,
                RoleName = "Student"
            }
         );

            // Admin rights

            context.Rights.AddOrUpdate(x => x.RightID, new Right
            {
                RightID = 1,
                RightName = "Create"
            });

            context.Rights.AddOrUpdate(x => x.RightID, new Right
            {
                RightID = 2,
                RightName = "Read"
            });

            context.Rights.AddOrUpdate(x => x.RightID, new Right
            {
                RightID = 3,
                RightName = "Update"
            });

            context.Rights.AddOrUpdate(x => x.RightID, new Right
            {
                RightID = 4,
                RightName = "Delete"
            });

            //context.Concepts.AddOrUpdate(new Concept { ConceptID = 1, Title = "Signifier", Courses = new List<Course>() { new Course { CourseID = 1, CourseName = "Interface Design" } } });
            ////context.Concepts.AddOrUpdate(new Concept { ConceptID = 2, Title = "Feedback", Sources = new List<Source>() { new Source { Arthor = "Allan cooper", SourceName = "Aboutface" } } });

            //context.Subjects.AddOrUpdate(new Subject { SubjectID = 1, Content = "dd" });


            context.Courses.AddOrUpdate(new Course { CourseID = 1, CourseName = "Interface Design" });
            context.Courses.AddOrUpdate(new Course { CourseID = 2, CourseName = "Backend" });
            context.Courses.AddOrUpdate(new Course { CourseID = 3, CourseName = "Frontend" });
            context.Courses.AddOrUpdate(new Course { CourseID = 4, CourseName = "Database" });
            context.Courses.AddOrUpdate(new Course { CourseID = 5, CourseName = "Communication" });
            context.Courses.AddOrUpdate(new Course { CourseID = 6, CourseName = "Interaction" });
            context.Courses.AddOrUpdate(new Course { CourseID = 7, CourseName = "Business" });
            context.Courses.AddOrUpdate(new Course { CourseID = 8, CourseName = "Design" });
            context.Courses.AddOrUpdate(new Course { CourseID = 9, CourseName = "Mobile Concept" });
            context.Courses.AddOrUpdate(new Course { CourseID = 10, CourseName = "Systemdesign" });
            context.Courses.AddOrUpdate(new Course { CourseID = 11, CourseName = "Android" });
            context.Courses.AddOrUpdate(new Course { CourseID = 12, CourseName = "Frameworks" });
            context.Courses.AddOrUpdate(new Course { CourseID = 13, CourseName = "IOS" });
            context.Courses.AddOrUpdate(new Course { CourseID = 14, CourseName = "Augmented Reality" });
            context.Courses.AddOrUpdate(new Course { CourseID = 15, CourseName = "Workenvironments" });


            //context.Sources.AddOrUpdate(new Source { SourceID = 1, Arthor = "Alan Cooper", SourceName = "Aboutface" });
            //context.Sources.AddOrUpdate(new Source { SourceID = 2, Arthor = "Sarah D", SourceName = "Ux personas" });
            //context.Sources.AddOrUpdate(new Source { SourceID = 3, Arthor = "Jose Torre", SourceName = "10 basic rules of UX" });


            context.SourceTypes.AddOrUpdate(new SourceType { SourceTypeID = 1, SourceTypeName = "Bog" });
            context.SourceTypes.AddOrUpdate(new SourceType { SourceTypeID = 2, SourceTypeName = "Url" });
            context.SourceTypes.AddOrUpdate(new SourceType { SourceTypeID = 3, SourceTypeName = "Link" });

            context.SourceTypeAttributes.AddOrUpdate(new SourceTypeAttribute { SourceTypeAttributeID = 1, SourceTypeAttributeName = "Side start", SourceTypeAttributeCatID = 1 });
            context.SourceTypeAttributes.AddOrUpdate(new SourceTypeAttribute { SourceTypeAttributeID = 2, SourceTypeAttributeName = "Side slut", SourceTypeAttributeCatID = 1 });
            context.SourceTypeAttributes.AddOrUpdate(new SourceTypeAttribute { SourceTypeAttributeID = 3, SourceTypeAttributeName = "URL", SourceTypeAttributeCatID = 2 });
            context.SourceTypeAttributes.AddOrUpdate(new SourceTypeAttribute { SourceTypeAttributeID = 4, SourceTypeAttributeName = "Time Stamp", SourceTypeAttributeCatID = 3 });





            context.SaveChanges();

        }
    }
}