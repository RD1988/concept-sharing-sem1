﻿using concept_sharing_sem1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Data.Entity;

namespace concept_sharing_sem1
{
    public class MvcApplication : System.Web.HttpApplication
    {

        private const String ReturnUrlRegexPattern = @"\?ReturnUrl=.*$";

        public MvcApplication()
        {
            PreSendRequestHeaders += MvcApplicationOnPreSendRequestHeaders;
        }

        private void MvcApplicationOnPreSendRequestHeaders(object sender, EventArgs e)
        {
            String redirectUrl = Response.RedirectLocation;

            if (String.IsNullOrEmpty(redirectUrl) || !Regex.IsMatch(redirectUrl, ReturnUrlRegexPattern))
            {

                return;

            }

            Response.RedirectLocation = Regex.Replace(redirectUrl, ReturnUrlRegexPattern, String.Empty);


        }



        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }



        protected void FormsAuthentication_OnAuthenticate(Object sender, FormsAuthenticationEventArgs e)
        {


            if (FormsAuthentication.CookiesSupported == true)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        string Email = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                        string roles = string.Empty;

                        using (Connection db = new Connection())
                        {
                          
                            User user = db.Users.Include(x => x.Role).SingleOrDefault(a => a.Email == Email);
                            roles = user.Role.RoleID.ToString();
                        }

                        e.User = new System.Security.Principal.GenericPrincipal(
                        new System.Security.Principal.GenericIdentity(Email, "Forms"), roles.Split(';'));


                    }
                    catch (Exception)
                    {

                        // throw;
                    }
                }
            }

        }


    }


}
