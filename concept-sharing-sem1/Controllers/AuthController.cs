﻿using concept_sharing_sem1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Data.Entity;

namespace concept_sharing_sem1.Controllers
{
    public class AuthController : Controller
    {

        private Connection db = new Connection();

        [Route("logind")]
        // GET: Auth login
        public ActionResult Logind()
        {
            return View();
        }




        [HttpPost]
        [Route("logind")]
        public ActionResult Logind(User model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                using (Connection db = new Connection())
                {
                    string Email = model.Email;
                    string Password = model.Password;


                    bool isUserValid = db.Users.Any(user => user.Email == Email && user.Password == Password);

                    if (isUserValid)
                    {
                        FormsAuthentication.SetAuthCookie(Email, false);


                        if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                           && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                        {
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            return RedirectToAction("Role", "Auth");
                        }

                    }
                    else
                    {
                        ViewBag.Error = "Ugyldig e-mail eller adgangskode";
                    }


                }
            }


            return View(model);

        }



        public ActionResult Role()
        {

            //Request cookie and decrypt it, so the system can see the e-mail
            string Email = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;

            //Open connection
            using (Connection db = new Connection())
            {

                var user = db.Users.Include(x => x.Role).FirstOrDefault(a => a.Email == Email);


                //Where goes the user after login, the role is in charge here..

                if (user.Role.RoleID == 1)
                {
                    //If role is equal to 1, we are looking for action called Index, and a Controller called Administrator
                    return RedirectToAction("Index", "Administrator");

                }

          

                if (user.Role.RoleID == 2 || user.Role.RoleID == 3)
                {
                    return RedirectToAction("Index", "Home");

                }


            }


            return View();
        }



        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }


    }
}


