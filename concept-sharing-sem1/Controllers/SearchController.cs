﻿using concept_sharing_sem1.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace concept_sharing_sem1.Controllers
{
    public class SearchController : Controller
    {
        Connection db = new Connection();

        [HttpPost] 
        public JsonResult Search(string searchQuery)
        {
            //check if subject exist in the database..
            Connection db = new Connection();

            var title = new SqlParameter("@Title", searchQuery);
            var course = new SqlParameter("@CourseName", searchQuery);

            var subjects = (from Result in db.Database.SqlQuery<SearchResult>("[getSearchBySubjectOrCourse] @Title, @CourseName", title, course)
                            where Result.Title.StartsWith(searchQuery)

                            select new
                            { 
                                label = Result.Title + "  |  " + Result.CourseName,
                                val = Result.SubjectID

                            }).ToList();


            return Json(subjects);
            
        }

        [HttpPost]
        public JsonResult Result(string searchQuery)
        {
         

                Session["getID"] = searchQuery;


                return Json(new { success = true, message = "Order updated successfully" }, JsonRequestBehavior.AllowGet);

   
          
        }


        public ActionResult Result(int? ID)
        {
            try
            {
                if(ID == null)
                {
                    Response.Redirect("www.google.dk");
                }

                 ID = Convert.ToInt32(Session["getID"]);

            

                Subject subject = new Subject();



                subject = db.Subjects
                .Include(x => x.Concept)
                .Include(x => x.Concept.course)
                .Include(x => x.Concept.Sources)
                .SingleOrDefault(x => x.SubjectID == ID);

                return View(subject);

            }
            catch (Exception)
            {


                return RedirectToAction("NotFound", "Search");


                throw;
            }
           



        }

        public ActionResult NotFound()
        {
            return View();
        }
    }
}