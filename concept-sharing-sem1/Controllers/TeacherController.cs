﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace concept_sharing_sem1.Controllers
{
    public class TeacherController : Controller
    {
        [Authorize(Roles = "2")]
        public ActionResult Index()
        {
            return View();
        }
    }
}