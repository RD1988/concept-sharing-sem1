﻿using concept_sharing_sem1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using concept_sharing_sem1.ViewModels;
using System.Data.SqlClient;

namespace concept_sharing_sem1.Controllers
{
    public class AdministratorController : Controller
    {
        // GET: Administrator
        private Connection db = new Connection();

        [Authorize(Roles = "1")]
        public ActionResult Index(string sortOrder, string searchString)
        {
            var UserList = db.Users.Include(x => x.Role).ToList();

            ViewBag.UserNameSort = String.IsNullOrEmpty(sortOrder) ? "Username" : "";
            ViewBag.EmailSort = String.IsNullOrEmpty(sortOrder) ? "Email" : "";
            ViewBag.RoleSort = String.IsNullOrEmpty(sortOrder) ? "Role" : "";
            

            if (!String.IsNullOrEmpty(searchString))
            {                
                UserList = db.Users.Include(x => x.Role).Where(x => x.Username.Contains(searchString)).ToList();
            }

            switch (sortOrder)
            {
                case "Username":
                    UserList = db.Users.Include(x => x.Role).OrderByDescending(x => x.Username).ToList();
                    break;
                case "Role":
                    //role virker ikke, tror det er pga forreign key selvom jeg .includer den
                    UserList = db.Users.Include(x => x.Role).OrderByDescending(x => x.Role).ToList();
                    break;

                case "Email":
                    UserList = db.Users.Include(x => x.Role).OrderByDescending(x => x.Email).ToList();
                    break;
                default:
                    //UserList = db.Users.Include(x => x.Role).OrderByDescending(x => x.Username).ToList();
                    //kan ikke få default til at virke da den overksriver de andre
                    break;
            }
                   
            
            return View(UserList);
        }
        public ActionResult DeleteUser(FormCollection fcNotUsed, int id = 0)
        {
            User user= db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult DeleteConcept(FormCollection fcNotUsed, int id = 0)
        {
            Concept delconcept = db.Concepts.Find(id);
            Source delsource = db.Sources.Find(id);
            Subject delsubject = db.Subjects.Find(id);
            if (delconcept== null)
            {
                return HttpNotFound();
            }
            db.Sources.Remove(delsource);
            db.Subjects.Remove(delsubject);
            db.Concepts.Remove(delconcept);
            db.SaveChanges();
    

            return RedirectToAction("AdminConcept");
        }


        public ActionResult Create()
        {


            return View();
        }
        public ActionResult AdminConcept(string ConceptSearch)
        {

            var ConceptList = db.Concepts.Include(x => x.subject).ToList();

           


            if (!String.IsNullOrEmpty(ConceptSearch))
            {
                ConceptList = db.Concepts.Include(x => x.subject).Where(x => x.subject.Title.Contains(ConceptSearch)).ToList();
            }

      

            return View(ConceptList);
        }
        //public ViewResult gotoconcept()
        //{
        //    return RedirectToAction();
        //}
    }
}