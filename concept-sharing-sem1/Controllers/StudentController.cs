﻿using concept_sharing_sem1.Models;
using concept_sharing_sem1.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

using static concept_sharing_sem1.ViewModels.ConceptViewModel;
using System.Web.UI.WebControls;
using System.Net;

namespace concept_sharing_sem1.Controllers
{

    [Authorize(Roles = "1,2,3")]
    public class StudentController : Controller
    {
        private Connection db = new Connection();


     
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult DoTitleExist(string title)
        {
            //
            bool isExist = db.Subjects.Where(u => u.Title.ToLowerInvariant().Equals(title.ToLower())).FirstOrDefault() != null;
            return Json(!isExist, JsonRequestBehavior.AllowGet);
        }


        [Route("opret-begreb")]
        public ActionResult Create()
        {
            ViewBag.SourceTypeID = new SelectList(db.SourceTypes, "SourceTypeID", "SourceTypeName");
            ViewBag.CourseID = new SelectList(db.Courses, "CourseID", "CourseName");

            

            return View();
        }

        public JsonResult getSourceTypes(int id)
        {
           
            var sourceTypesCat = db.SourceTypeAttributes.Where(x => x.SourceTypeAttributeCatID == id).ToList();

            List<TextBox> liSourceTypesCat = new List<TextBox>();

            if (sourceTypesCat != null)
            {


                foreach (var x in sourceTypesCat)
                {

                    liSourceTypesCat.Add(new TextBox { Text = x.SourceTypeAttributeName, ID = x.SourceTypeAttributeID.ToString() });
                }
            }

            return Json(liSourceTypesCat, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getSourceTypesInput(string getSourceTypeID, string getCourseTypeID)
        {



            return Json(new { success = true, message = "Order updated successfully" }, JsonRequestBehavior.AllowGet);
        }




        //Source sources,
        [Route("opret-begreb")]
        [HttpPost]
        public ActionResult Create(ConceptViewModel model, string getSourceTypeID, string getCourseTypeID)
        {
            
            ViewBag.CourseID = new SelectList(db.Courses).SelectedValue;
            ViewBag.SourceTypeID = new SelectList(db.SourceTypes).SelectedValue;

            int id;

            if (db.Concepts.Max(x => x.SourceID) == null)
            {
                id = 1;
            
            }

            else
            {
                id = db.Concepts.Max(x => x.SourceID).Value;

                id = id + 1;
            }
            
            if (ModelState.IsValid)
            {
              
                var concept = new Concept()
                {
                    Content = model.Content,
                    CourseID = Convert.ToInt32(getCourseTypeID),
                    SourceID = id

                };

                var subject = new Subject()
                {

                    Title = model.Title
                };



                var source = new Source()
                {
                    Arthor = model.Arthor,
                    SourceName = model.SourceName,
                    SourceTypeID = Convert.ToInt32(getSourceTypeID)

                };

                
                // In concept we have a ref called concept, thats is ref to subject
                concept.subject = subject;

                source.concept = concept;

                db.Concepts.Add(concept);

                db.Sources.Add(source);

                ViewBag.SourceTypeID = new SelectList(db.SourceTypes, "SourceTypeID", "SourceTypeID"); 
                ViewBag.CourseID = new SelectList(db.Courses, "CourseID", "CourseName");

                db.SaveChanges();

            }


           
            return View();
        }


        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

           Subject subject = db.Subjects.Include(x => x.Concept).SingleOrDefault(x=> x.SubjectID == id);
            if (subject == null)
            {
                return HttpNotFound();
            }

            return View(subject);
        }

        [HttpPost]
        public ActionResult Edit(Subject model)
        {
     
            db.Database.ExecuteSqlCommand("UPDATE con SET con.Content = '" + model.Concept.Content +"' FROM Subjects AS  sub INNER JOIN Concepts AS con ON sub.SubjectID = con.ConceptID INNER JOIN Courses AS cou ON con.CourseID = cou.CourseID INNER JOIN Sources AS sou ON con.SourceID = sou.ConceptID WHERE sub.SubjectID = " + model.SubjectID);
            db.SaveChanges();


            return View(model);
        }





        }
}