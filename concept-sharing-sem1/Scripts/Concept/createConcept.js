﻿function getCourcetypes()
{
    $("#courseTypes").change(function (event)
    {
        $('input[name=getCourseTypeID]').val($('#courseTypes').val());
    });
}

function getSourcetypes()
{
    $(document).ready(function ()
    {

        $("#sourceTypes").change(function (event) {

            //Show the Author and 
            document.getElementById('authorTitle').style.display = 'block';
            document.getElementById('Attributes').style.display = 'block';

            //Set the value to the input field then the dropdown change
            $('input[name=getSourceTypeID]').val($('#sourceTypes').val());

            $("#sourceTypesAttributes").empty();
            $.ajax({
                type: 'POST',
                url: '/Student/getSourceTypes',
                dataType: 'json',
                data: { id: $("#sourceTypes").val() },

                success: function (sourceTypesAttributes) {

                    $.each(sourceTypesAttributes, function (i, sourceTypesAttributes) { //Dont put the onclick function in here, it repeats itself!!!

                        $("#sourceTypesAttributes").append('<input class="form-control" id="' + sourceTypesAttributes.ID + '" name="' + sourceTypesAttributes.Text + '" placeholder="' + sourceTypesAttributes.Text + '" required="required" type="text" ><br/>');

                    });

                    $("#addSource").on("click", function ()
                    {

                        //var pagestart = $('#1').val();
                        //var pageend = $('#2').val();
                        //var linkurl = $('#3').val();
                        //var timestamp = $('#4').val();

                        //var bookArr = [];
                        //bookArr.push(pagestart + "," + pageend);

                        //var unique = bookArr.filter(function (itm, i, bookArr) {
                        //    return i === bookArr.indexOf(itm);
                        //});

                        //console.log(unique);



                    });


                },
                error: function (ex) {
                    alert('Failed to retrieve states.' + ex);
                }
            });
            return false;







            //End
        })
    });
}




function postFormConcept()
{
    var form = document.getElementById('submitForm');
    var createcrateConcept = jQuery('#crateConcept').attr("data-attribute");
    form.addEventListener('submit', function (event) {
        if (!event.target.checkValidity())
        {
            event.preventDefault(); // dismiss the default functionality




            // error message
            return true;
        }
        else
        {
            var getSourceTypeID = $('input[name=getSourceTypeID]').val();
            var getCourseTypeID = $('input[name=getCourseTypeID]').val();

            $.ajax({
                type: 'POST',
                url: '/Student/getSourceTypesInput',
                dataType: 'json',
                data: { getSourceTypeID : getSourceTypeID, getCourseTypeID: getCourseTypeID },
                success: function (data)
                {
                    if (data.success)
                    {
                        //alert(getID);
                    }
                }
            });


            $.post(createcrateConcept);
        }

    }, false);
}

