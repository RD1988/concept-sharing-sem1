﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace concept_sharing_sem1.Models
{
    public class Connection : DbContext
    {
        //Construker for the connection to the database refered in the webconfig file.
        public Connection() : base("Connection")
        {

        }

        //DBset is equal to the database tables in the database. ASK if you have any questions...

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Right> Rights { get; set; }
        public virtual DbSet<Concept> Concepts { get; set; }
        public virtual DbSet<Course> Courses { get; set; }
        public virtual DbSet<Source> Sources { get; set; }
        public virtual DbSet<SourceType> SourceTypes { get; set; }
        public virtual DbSet<SourceTypeAttribute> SourceTypeAttributes { get; set; }
        public virtual DbSet<Subject> Subjects { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Subject>().HasRequired(u => u.Concept).WithRequiredDependent(u => u.subject).WillCascadeOnDelete(true);
            modelBuilder.Entity<Concept>().HasRequired(u => u.subject).WithRequiredPrincipal(u => u.Concept).WillCascadeOnDelete(true);
            modelBuilder.Entity<Concept>().HasRequired(u => u.course).WithMany(u => u.concept).WillCascadeOnDelete(true);



            //One source can have one or more sourcetypes attached to it. We have a source, the source type to be a book, a url or a video.

            //modelBuilder.Entity<Source>()
            //    .HasRequired<SourceType>(s => s.)
            //    .WithMany(g => g.Sources)
            //    .HasForeignKey<int>(s=>s.sourceTypeID);












        }


    }
    }