﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace concept_sharing_sem1.Models
{
    public class Course
    {
        public int CourseID { get; set; }
        public string CourseName { get; set; }

        public ICollection<Concept> concept { get; set; }
    }
}