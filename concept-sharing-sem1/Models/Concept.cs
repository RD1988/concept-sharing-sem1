﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace concept_sharing_sem1.Models
{
    public class Concept
    {
        public Concept()
        {
            this.user = new HashSet<User>();
           
            
        }
        public int ConceptID { get; set; }
        [AllowHtml]
        public string Content { get; set; }
        public string JsonPath { get; set; }



        ////relation to concept
        //public int CurrentSubjectId { get; set; }
        public  Subject subject { get; set; }



        public int? CourseID { get; set; }
        public Course course { get; set; }


        // Many to many
        public ICollection<User> user { get; set; }

        public int? SourceID { get; set; }
        public ICollection<Source> Sources { get; set; }

    }
}