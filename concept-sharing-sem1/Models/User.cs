﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace concept_sharing_sem1.Models
{
    public class User
    {
        public User()
        {
            this.concept = new HashSet<Concept>();
        }

        //This is a propertie. Equals to the columns in the database.
        public int UserID { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }


        [Index("EmailIndex", IsUnique = true)]
        [StringLength(150)]
        public string Email { get; set; }

        //ref to role and it called Role. Also called reference navigation property
        public int? RoleID { get; set; }
        public Role Role { get; set; }
            

        // Many to many from user to concept
         public ICollection<Concept> concept { get; set; }



    }
}