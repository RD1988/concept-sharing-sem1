﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace concept_sharing_sem1.Models
{
    public class Role
    {
        public Role()
        {
            this.right = new HashSet<Right>(); 
        }

        public int RoleID { get; set; }
        public string RoleName { get; set; }


        //This means that the role can go to many users = 1..*
        public ICollection<User> user { get; set; }

        //This means that the roles can have many to many rights attached to them
        public ICollection<Right> right { get; set; }
    }
}