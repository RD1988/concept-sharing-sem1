﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace concept_sharing_sem1.Models
{
    public class Right
    {
        public Right()
        {
            this.role = new HashSet<Role>();
        }

        public int RightID { get; set; }
        public string RightName { get; set; }

        //This means that the rights can have many to many roles attached to them
        public virtual ICollection<Role> role { get; set; }
    }
}