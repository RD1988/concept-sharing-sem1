﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace concept_sharing_sem1.Models
{
    public class Subject
    {
        public int SubjectID { get; set; }

        [Required]
        [Index("TitleIndex", IsUnique = true)]
        [StringLength(150)]
     
        public string Title { get; set; }


        public Concept Concept { get; set; }
    }
}