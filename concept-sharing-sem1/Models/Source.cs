﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace concept_sharing_sem1.Models
{
    public class Source
    {
        public int SourceID { get; set; }
        public string Arthor { get; set; }
        public string SourceName { get; set; }

        public int? ConceptID { get; set; }
        public Concept concept { get; set; }


        public int? SourceTypeID { get; set; }
        public SourceType SourceType { get; set; }
       


    }
}