﻿using concept_sharing_sem1.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace concept_sharing_sem1.ViewModels
{

    public class ConceptViewModel
    {
        // To find the subject
        public int SubjectID { get; set; }
        public Subject subject { get; set; } 
        public Concept concept { get; set; }
        

        //Concept
        [Required]
        [Display(Name = "Begreb title")]
        [Remote("DoTitleExist", "Validation", ErrorMessage = "EmialId is already exist", AdditionalFields = "Title")]
        public string Title { get; set; }

        [AllowHtml]
        [Required]
        [DisplayName("Begreb beskrivelse")]
        public string Content { get; set; }

        //Courses
        public int CourseID { get; set; }
        [DisplayName("Fag")]
        public string CourseName { get; set; }

        //Sources
        public int? SourceID { get; set; }
        [DisplayName("Forfatter")]
        public string Arthor { get; set; }

        [DisplayName("Kilde typer")]
        public string SourceName { get; set; }

        public int SourceTypeID { get; set; }
        //from the hidden field
        public int? GetID { get; set; }

        public string SourceTypeName { get; set; }

        //SourcesTypesAtrributes
        public int SourceTypeAttributeID { get; set; }
        public int SourceTypeAttributeCatID { get; set; }
        public string SourceTypeAttributeName { get; set; }
        

    }

 
}